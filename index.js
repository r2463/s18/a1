


let trainer = {};

console.log(trainer)


trainer.Name = "Ash Ketchum"
trainer.age = 10
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"]
trainer.friends = {hoenn: ["May", "Max"], kanto: ["Brock", "Misty"]}


trainer.talk = function(shout){

		return `Pikachu, I choose you!`

}



console.log()



console.log(`Result of dot notation:`)
console.log(trainer.Name)

console.log(`Result of square bracket notation:`)
console.log(trainer.pokemon)

console.log(`Result of talk method`)
console.log(trainer.talk())





function Pokemon(name, lvl, health, attack){
	this.name = name;
	this.level = lvl;
	this.health = health
	this.attack = lvl
	this.tackle = function(opponent){

		(this.health) -= (opponent.attack)
		console.log(`${opponent.name} tackled ${this.name}`)
		console.log(`${this.name}'s health is now reduced to ${this.health}`)

		if(this.health <= 0){
			this.faint()
		}
	}

	this.faint = function (){

		console.log(`${this.name} has fainted`)
	}


}



let pikachu = new Pokemon("Pikachu", 12, 24, 12);
let geodude = new Pokemon("Geodude", 8, 16, 8);
let mewtwo = new Pokemon("Mewtwo", 100, 200, 100);



console.log(pikachu)
console.log(geodude)
console.log(mewtwo)



pikachu.tackle(geodude);
console.log(pikachu)


geodude.tackle(mewtwo);
console.log(geodude)
















